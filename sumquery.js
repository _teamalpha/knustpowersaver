var request = require("request");

var options = { method: 'POST',
  url: 'https://time-series-store-predix.run.aws-usw02-pr.ice.predix.io/v1/datapoints',
  headers: 
   { 'postman-token': '6081e772-9627-4930-df72-3c726a2e1141',
     'cache-control': 'no-cache',
     'content-type': 'application/json',
     'predix-zone-id': '06dabe5a-29fb-4821-b71a-4d0aa2881354',
     authorization: 'bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImxlZ2FjeS10b2tlbi1rZXkiLCJ0eXAiOiJKV1QifQ.eyJqdGkiOiJhZDE5MzY1ZTI0Yzg0OTRlYTc1OTdlYjE4ZTcyMWRkZiIsInN1YiI6InBvd2VyX3NlcnZpY2VzIiwic2NvcGUiOlsiem9uZXMucmVhZCIsInpvbmVzLjE1MjAxMDlmLTZmMWUtNDg5OC05OWJmLWE2ZDk2ZDIwMjQyMC5yZWFkIiwiaWRwcy53cml0ZSIsInVhYS5yZXNvdXJjZSIsInRpbWVzZXJpZXMuem9uZXMuMDZkYWJlNWEtMjlmYi00ODIxLWI3MWEtNGQwYWEyODgxMzU0LnF1ZXJ5Iiwic2NpbS5yZWFkIiwic2NpbS5jcmVhdGUiLCJ0aW1lc2VyaWVzLnpvbmVzLjA2ZGFiZTVhLTI5ZmItNDgyMS1iNzFhLTRkMGFhMjg4MTM1NC51c2VyIiwidWFhLnVzZXIiLCJ6b25lcy4xNTIwMTA5Zi02ZjFlLTQ4OTgtOTliZi1hNmQ5NmQyMDI0MjAuY2xpZW50cy53cml0ZSIsInVhYS5ub25lIiwiem9uZXMud3JpdGUiLCJ6b25lcy4xNTIwMTA5Zi02ZjFlLTQ4OTgtOTliZi1hNmQ5NmQyMDI0MjAuY2xpZW50cy5yZWFkIiwidGltZXNlcmllcy56b25lcy4wNmRhYmU1YS0yOWZiLTQ4MjEtYjcxYS00ZDBhYTI4ODEzNTQuaW5nZXN0IiwiaWRwcy5yZWFkIiwic2NpbS53cml0ZSIsInpvbmVzLjE1MjAxMDlmLTZmMWUtNDg5OC05OWJmLWE2ZDk2ZDIwMjQyMC5hZG1pbiJdLCJjbGllbnRfaWQiOiJwb3dlcl9zZXJ2aWNlcyIsImNpZCI6InBvd2VyX3NlcnZpY2VzIiwiYXpwIjoicG93ZXJfc2VydmljZXMiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6ImFhN2FlMjg5IiwiaWF0IjoxNDk3NDU1NjQ2LCJleHAiOjE0OTc0OTg4NDYsImlzcyI6Imh0dHBzOi8vYjYxOTYwNTctMTczMC00ZTI3LTk5OWItNDUyMWQ0NGRjNWM1LnByZWRpeC11YWEucnVuLmF3cy11c3cwMi1wci5pY2UucHJlZGl4LmlvL29hdXRoL3Rva2VuIiwiemlkIjoiYjYxOTYwNTctMTczMC00ZTI3LTk5OWItNDUyMWQ0NGRjNWM1IiwiYXVkIjpbInRpbWVzZXJpZXMuem9uZXMuMDZkYWJlNWEtMjlmYi00ODIxLWI3MWEtNGQwYWEyODgxMzU0Iiwic2NpbSIsInVhYSIsInpvbmVzLjE1MjAxMDlmLTZmMWUtNDg5OC05OWJmLWE2ZDk2ZDIwMjQyMC5jbGllbnRzIiwiem9uZXMiLCJ6b25lcy4xNTIwMTA5Zi02ZjFlLTQ4OTgtOTliZi1hNmQ5NmQyMDI0MjAiLCJwb3dlcl9zZXJ2aWNlcyIsImlkcHMiXX0.NM5ZGxEPo9bGr1XRM51ncs_wA4rweHm6w2WKd2EVquBR5u81_utNozxSat1eVJH20yxSO7RwZinLEKiAXuSganiNKp5ywoRuLIugZgrqO4C3pAy6r0EBh-l78fpbM86RPU1ZxCQYG4FHc12OyWnk0e-WHHdFbDBgqoPDwOALLdO8UfSObFD251kpbDjnMpJu71mMOGBIyqKQc19G0hQM8mxfzO9GRlCi-G8dBD4c_eRlxS1k1dw-so43vvY62iK4dcL_tFzZmFeawGGdqqQthvOpFkl5PV8jpzxaUs-sRVEw23pjWYFILagxLc5LiI12jYFiTi9NntZ2Gd608MyHIw' },
  body: 
   { tags: 
      [ { name: 'Samuel Sonia Smart Bio',
          order: 'desc',
          aggregations: [ { type: 'sum', sampling: { unit: 's', value: '30' } } ] } ],
     start: 1496332847000,
     end: 1497456047000 },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body.tags[0].results[0].values[0][1]);
});



// (new Date).getTime() - 2592000000)
// (new Date).getTime()