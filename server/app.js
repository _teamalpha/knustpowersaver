const pg = require('pg');
var express = require('express');
var path = require('path'), fs = require("fs");
var cookieParser = require('cookie-parser'); // used for session cookie
var bodyParser = require('body-parser');
// simple in-memory session is used here. use connect-redis for production!!
var session = require('express-session');
var proxy = require('./routes/proxy'); // used when requesting data from real services.

var index = require('./routes/index');

//custom
var request = require('request');
var ta = require('time-ago');

// get config settings from local file or VCAPS env var in the cloud
var config = require('./predix-config');

var passport;  // only used if you have configured properties for UAA
// configure passport for oauth authentication with UAA
var passportConfig = require('./passport-config');

// if running locally, we need to set up the proxy from local config file:
var node_env = process.env.node_env || 'development';
if (node_env === 'development') {
  var devConfig = require('./localConfig.json')[node_env];
	proxy.setServiceConfig(config.buildVcapObjectFromLocalConfig(devConfig));
	proxy.setUaaConfig(devConfig);
}

//a back-end java microservice used in the Build A Basic App learningpath
var windServiceURL = devConfig ? devConfig.windServiceURL : process.env.windServiceURL;

console.log('************'+node_env+'******************');

if (config.isUaaConfigured()) {
	passport = passportConfig.configurePassportStrategy(config);
}

//turns on or off text or links depending on which tutorial you are in, guides you to the next tutorial
var learningpaths = require('./learningpaths/learningpaths.js');

/**********************************************************************
       SETTING UP EXRESS SERVER
***********************************************************************/
var app = express();

app.set('trust proxy', 1);

// set the view engine to pug
app.set('views', [path.join(__dirname + '/../views'), path.join(__dirname + '/../secure')]);
app.engine('html', require('pug').renderFile);

app.use(cookieParser('predixsample'));
// Initializing default session store
// *** Use this in-memory session store for development only. Use redis for prod. **

app.use(session({
	secret: 'predixsample',
	name: 'cookie_name',
	proxy: true,
	resave: true,
	saveUninitialized: true}));

if (config.isUaaConfigured()) {
	app.use(passport.initialize());
  // Also use passport.session() middleware, to support persistent login sessions (recommended).
	app.use(passport.session());
}

//Initializing application modules
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var server = app.listen(process.env.VCAP_APP_PORT || 5000, function () {
	console.log ('Server started on port: ' + server.address().port);
});

/*******************************************************
SET UP MOCK API ROUTES
*******************************************************/
// Import route modules
// var viewServiceRoutes = require('./routes/view-service-routes.js')();
// var assetRoutes = require('./routes/predix-asset-routes.js')();
// var timeSeriesRoutes = require('./routes/time-series-routes.js')();

// add mock API routes.  (Remove these before deploying to production.)
//app.use('/api/view-service', jsonServer.router(viewServiceRoutes));
//app.use('/api/predix-asset', jsonServer.router(assetRoutes));
//app.use('/api/time-series', jsonServer.router(timeSeriesRoutes));

/****************************************************************************
	SET UP EXPRESS ROUTES
*****************************************************************************/




app.use(express.static(path.join(__dirname, process.env['base-dir'] ? process.env['base-dir'] : '../public')));





/*****************************************************************************
 * Setup Postgresql connection
 *****************************************************************************/

// const pg = require('pg');
// const connection_string = process.env.DATABASE_URL || 'postgres://unfx9hv4z5npqrs1:m6xnboe8cnq9w2b67st36onam@db-ff722de2-c183-4907-b31e-e8c029c66d47.c7uxaqxgfov3.us-west-2.rds.amazonaws.com:5432/postgres';

// const client = new pg.Client(connection_string);
// client.connect();
// // const query = client.query('CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
// // TODO: Query multiple times before ending the client session
// const query = client.query('CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
// query.on('end', () => { client.end(); });



var timeseries_options = { method: 'POST',
  url: 'https://time-series-store-predix.run.aws-usw02-pr.ice.predix.io/v1/datapoints',
  headers: 
   { 'postman-token': '6081e772-9627-4930-df72-3c726a2e1141',
     'cache-control': 'no-cache',
     'content-type': 'application/json',
     'predix-zone-id': '06dabe5a-29fb-4821-b71a-4d0aa2881354',
     authorization: 'bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImxlZ2FjeS10b2tlbi1rZXkiLCJ0eXAiOiJKV1QifQ.eyJqdGkiOiI5Y2RkZWIxNmY4ODA0YzM2YjliYjdiNThjMWI4YjJhZiIsInN1YiI6InBvd2VyX3NlcnZpY2VzIiwic2NvcGUiOlsiem9uZXMucmVhZCIsInpvbmVzLjE1MjAxMDlmLTZmMWUtNDg5OC05OWJmLWE2ZDk2ZDIwMjQyMC5yZWFkIiwiaWRwcy53cml0ZSIsInVhYS5yZXNvdXJjZSIsInRpbWVzZXJpZXMuem9uZXMuMDZkYWJlNWEtMjlmYi00ODIxLWI3MWEtNGQwYWEyODgxMzU0LnF1ZXJ5Iiwic2NpbS5yZWFkIiwic2NpbS5jcmVhdGUiLCJ0aW1lc2VyaWVzLnpvbmVzLjA2ZGFiZTVhLTI5ZmItNDgyMS1iNzFhLTRkMGFhMjg4MTM1NC51c2VyIiwidWFhLnVzZXIiLCJ6b25lcy4xNTIwMTA5Zi02ZjFlLTQ4OTgtOTliZi1hNmQ5NmQyMDI0MjAuY2xpZW50cy53cml0ZSIsInVhYS5ub25lIiwiem9uZXMud3JpdGUiLCJ6b25lcy4xNTIwMTA5Zi02ZjFlLTQ4OTgtOTliZi1hNmQ5NmQyMDI0MjAuY2xpZW50cy5yZWFkIiwidGltZXNlcmllcy56b25lcy4wNmRhYmU1YS0yOWZiLTQ4MjEtYjcxYS00ZDBhYTI4ODEzNTQuaW5nZXN0IiwiaWRwcy5yZWFkIiwic2NpbS53cml0ZSIsInpvbmVzLjE1MjAxMDlmLTZmMWUtNDg5OC05OWJmLWE2ZDk2ZDIwMjQyMC5hZG1pbiJdLCJjbGllbnRfaWQiOiJwb3dlcl9zZXJ2aWNlcyIsImNpZCI6InBvd2VyX3NlcnZpY2VzIiwiYXpwIjoicG93ZXJfc2VydmljZXMiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6ImFhN2FlMjg5IiwiaWF0IjoxNDk3NTA1MzM4LCJleHAiOjE0OTc1NDg1MzgsImlzcyI6Imh0dHBzOi8vYjYxOTYwNTctMTczMC00ZTI3LTk5OWItNDUyMWQ0NGRjNWM1LnByZWRpeC11YWEucnVuLmF3cy11c3cwMi1wci5pY2UucHJlZGl4LmlvL29hdXRoL3Rva2VuIiwiemlkIjoiYjYxOTYwNTctMTczMC00ZTI3LTk5OWItNDUyMWQ0NGRjNWM1IiwiYXVkIjpbInRpbWVzZXJpZXMuem9uZXMuMDZkYWJlNWEtMjlmYi00ODIxLWI3MWEtNGQwYWEyODgxMzU0Iiwic2NpbSIsInVhYSIsInpvbmVzLjE1MjAxMDlmLTZmMWUtNDg5OC05OWJmLWE2ZDk2ZDIwMjQyMC5jbGllbnRzIiwiem9uZXMiLCJ6b25lcy4xNTIwMTA5Zi02ZjFlLTQ4OTgtOTliZi1hNmQ5NmQyMDI0MjAiLCJwb3dlcl9zZXJ2aWNlcyIsImlkcHMiXX0.UMqAMRYihiNn93pwl1JyGsM_b3OmqkGvwz5BTaoehtoaT1TGvXYxoM45kg75oJQk9YUvEzRyyvEOYQFyAryxGdOxEKn5e18uXvr8qDTgxM53PJxrR0LxkJqSb-Okhr1TcvRCtQ1rKhJQOJ7gM56hFXZh8iWliQSheJ8S23iwHenv2uAGcrcmoUTnR9lFFBHtNaaYEz-meQO5kYOrzuEsOmXFOnPRgrRA7Vq99msRuDMeFZv38RmKKDfkXXZaXVqMOsq7kt-iODoqhxaiYnh-t2zNOX9KC3yr40IT9HQ1d1Th6UjktwCfCvKZ9r5MJbegtpm3te18Xv5v91mGPjqpLg' },
  body: 
   { tags: 
      [ { name: 'Samuel Sonia Smart Bio',
          order: 'desc',
          aggregations: [ { type: 'sum', sampling: { unit: 's', value: '30' } } ] } ],
     start: 1496332847000, //a month ago
     end: 1497456047000 },
  json: true };



if (config.isUaaConfigured()) {
	//Use this route to make the entire app secure.  This forces login for any path in the entire app.
	app.use('/', index);
  //login route redirect to predix uaa login page
  app.get('/login',passport.authenticate('predix', {'scope': ''}), function(req, res) {
    // The request will be redirected to Predix for authentication, so this
    // function will not be called.
  });

  // access real Predix services using this route.
  // the proxy will add UAA token and Predix Zone ID.
  app.use('/predix-api',
  	passport.authenticate('main', {
  		noredirect: true
  	}),
  	proxy.router);

  //callback route redirects to secure route after login
  app.get('/callback', passport.authenticate('predix', {
  	failureRedirect: '/'
  }), function(req, res) {
  	console.log('Redirecting to secure route...');
  	res.redirect('/secure');
    });

  // example of calling a custom microservice.
  if (windServiceURL && windServiceURL.indexOf('https') === 0) {
    app.get('/api/services/windservices/*', passport.authenticate('main', { noredirect: true}),
      // if calling a secure microservice, you can use this middleware to add a client token.
      // proxy.addClientTokenMiddleware,
      proxy.customProxyMiddleware('/api', windServiceURL)
    );
  }

  /**
  ** this endpoint is required by Timeseries.js, for winddata is switch
  **/
    app.get('/config-details', passport.authenticate('main', {
      noredirect: true //Don't redirect a user to the authentication page, just show an error
      }), function(req, res) {
      console.log('Accessing the secure route data');
      res.setHeader('Content-Type', 'application/json');
      var configuration = {};
      if(!windServiceURL) {
        configuration.connectToTimeseries = "true";
      }
      if(config.assetURL && config.assetZoneId) {
        configuration.isConnectedAssetEnabled = "true";
      }
      res.send(JSON.stringify(configuration));

    });

  //Or you can follow this pattern to create secure routes,
  // if only some portions of the app are secure.
  app.get('/secure', passport.authenticate('main', {
    noredirect: true //Don't redirect a user to the authentication page, just show an error
    }), function(req, res) {
    console.log('Accessing the secure route');
    // modify this to send a secure.html file if desired.
    // res.sendFile(path.join(__dirname + '/../secure/secure.html'));

    request(timeseries_options, function (error, response, body) {
      if (error)
        return console.log('Error! Could not requst from time series' + error);

      console.log(body);
      console.log('\n\nresponse: ' + response);

      const connection_string = 'postgres://u0tgt71blhv3b1jk:u6i39w2a2bwzdojexbkewe169@db-da4df70d-86e4-492d-9bf0-7ffaf52bed2f.c7uxaqxgfov3.us-west-2.rds.amazonaws.com:5432/postgres';
      const client = new pg.Client(connection_string);
      client.connect();

      var _query = 'SELECT device_id FROM device';
      var num_dev = 0;
      var num_faults = 0;

      var query = client.query(_query, function (err1, result1) {
        if (err1) {
          res.send(err1.stack);
        }
        else {
          const client2 = new pg.Client(connection_string);
          client2.connect();

          _query = 'SELECT id FROM reports';

          query = client2.query(_query, function (err2, result2) {
            if (err2) {
              res.send(err2.stack);
            }
            else {
              num_dev = result1.rowCount;
              num_faults = result2.rowCount;

              res.render(path.join(__dirname + '/../secure/secure.pug'), {
                power_consumption: body.tags[0].results[0].values[0][1],
                energy_savings: 32,
                reported_faults: num_faults,
                num_of_devices: num_dev
              });
            }
            console.log(result2);
          });
        }
        console.log(result1);
      });

      // res.render(path.join(__dirname + '/../secure/secure.pug'), {
      //   power_consumption: body.tags[0].results[0].values[0][1],
      //   energy_savings: 32,
      //   reported_faults: 98549,
      //   num_of_devices: 33
      // });
    });
  });

  app.get('/secure/asset_management.html', passport.authenticate('main', {
    noredirect: true //Don't redirect a user to the authentication page, just show an error
    }), function(req, res) {
    console.log('Accessing the secure route');
    // modify this to send a secure.html file if desired.
    res.sendFile(path.join(__dirname + '/../secure/asset_management.html'));
  });

  app.get('/secure/add_device.html', passport.authenticate('main', {
    noredirect: true //Don't redirect a user to the authentication page, just show an error
  }), function(req, res) {

    const connection_string = 'postgres://u0tgt71blhv3b1jk:u6i39w2a2bwzdojexbkewe169@db-da4df70d-86e4-492d-9bf0-7ffaf52bed2f.c7uxaqxgfov3.us-west-2.rds.amazonaws.com:5432/postgres';
    const client = new pg.Client(connection_string);
    client.connect();

    const queryText = 'SELECT college_name FROM college';
    var college_names=[];

    var query = client.query(queryText, function(err, result){
      if(err){
        res.send(err.stack);
      }
      else{
        for(var i = 0; i < result.rows.length; i++){
          college_names.push((result.rows[i])['college_name']);
        }
        console.log('college_names: '+college_names);
        res.render(path.join(__dirname + '/../secure/add_device.pug'),
        {
          device_types: ['dev_1','dev_2','dev_3','dev_4'],
          colleges: college_names,
          buildings: ['building_1','building_2']
        }
        );
      }
      console.log(result);
    });


    console.log('Accessing the secure route');
    // modify this to send a secure.html file if desired.
    //res.sendFile(path.join(__dirname + '/../secure/_add_device.html'));
  });

  app.get('/secure/reports.html', passport.authenticate('main', {
    noredirect: true //Don't redirect a user to the authentication page, just show an error
  }), function (req, res) {
    console.log('Accessing the secure route');
    // modify this to send a secure.html file if desired.
    
    const connection_string = 'postgres://u0tgt71blhv3b1jk:u6i39w2a2bwzdojexbkewe169@db-da4df70d-86e4-492d-9bf0-7ffaf52bed2f.c7uxaqxgfov3.us-west-2.rds.amazonaws.com:5432/postgres';
    const client = new pg.Client(connection_string);
    client.connect();

    const query_for_id = 'SELECT id FROM reports';
    const query_for_desc = 'SELECT description FROM reports';
    const query_for_loc = 'SELECT location FROM reports';
    const query_for_solved = 'SELECT solved FROM reports';
    var ids = [], descs = [], locs = [], solves = [];

    var id_query = client.query(query_for_id, function(err, result){
      if(err){
        res.send(err.stack);
      }
      else{
        console.log('\n\nresult for id_query: '+result+'\n\n');
        for(var i = 0; i < result.rows.length; i++){
          ids.push(result.rows[i].id);
        }
      }
    });

    var desc_query = client.query(query_for_desc, function(err, result){
      if(err){
        res.send(err.stack);
      }
      else{
        console.log('\n\nresult for desc_query: '+result+'\n\n');
        for(var i = 0; i < result.rows.length; i++){
          descs.push(result.rows[i].description);
        }
      }
    });

    var loc_query = client.query(query_for_loc, function(err, result){
      if(err){
        res.send(err.stack);
      }
      else{
        console.log('\n\nresult for loc_query: '+result+'\n\n');
        for(var i = 0; i < result.rows.length; i++){
          locs.push(result.rows[i].location);
        }
      }
    });

    var solved_query = client.query(query_for_solved, function(err, result){
      if(err){
        res.send(err.stack);
      }
      else{
        console.log('\n\nresult for solved_query: '+result+'\n\n');
        for(var i = 0; i < result.rows.length; i++){
          solves.push(result.rows[i].solved);
        }
      }
    });

    setTimeout(function(){
      //package for the pug renderer
      var _obj = [];
      for(var j = 0; j < ids.length; j++){
      console.log(typeof solves);
      if(solves[j] == false)
      {
        var _tmp={'id':ids[j],'location':locs[j],'description':descs[j]};
        _obj.push(_tmp);
        console.log('_tmp.id = '+_tmp.id);
        console.log('_tmp.id = '+_tmp.location);
      }
      }

      console.log('ids: '+ids);
      console.log('descs: '+descs);
      console.log('solves: '+solves);      
      res.render(path.join(__dirname + '/../secure/reports.pug'),
        {
          reports: _obj
        }
    )},3000);

    // res.render(path.join(__dirname + '/../secure/reports.pug')
      //send various reports here
    // );
    // res.sendFile(path.join(__dirname + '/../secure/reports.html'));
  });

  app.get('/secure/about.html', passport.authenticate('main', {
    noredirect: true //Don't redirect a user to the authentication page, just show an error
    }), function(req, res) {
    console.log('Accessing the secure route');
    // modify this to send a secure.html file if desired.
    res.sendFile(path.join(__dirname + '/../secure/about.html'));
  });
}


//internal affairs only!!
// such as AJAX queries to the PostgreSQL DB servive
app.get('/internal_sql', passport.authenticate('main', {
    noredirect: true //Don't redirect a user to the authentication page, just show an error
    }), function(req, res, next){
      //this will be the sql query
      const _sql_query = req.query;

      console.log(req.query);

      //create connection and query DB
      const connection_string = 'postgres://u0tgt71blhv3b1jk:u6i39w2a2bwzdojexbkewe169@db-da4df70d-86e4-492d-9bf0-7ffaf52bed2f.c7uxaqxgfov3.us-west-2.rds.amazonaws.com:5432/postgres';
      const client = new pg.Client(connection_string);
      client.connect();

      var query = client.query(_sql_query, function (err, result) {
        if (err) {
          return res.send('ERROR processing an internal(SQL) proc: '+err.stack);
        }
        else {
          console.log('Internal proc result = '+result);

          //will be stringifyied JSON
          res.setHeader('Content-Type', 'text/plain');
          res.write(JSON.stringify(result));
          res.end();
          console.log(JSON.stringify(result));
        }
      });
});


//logout route
app.get('/logout', function(req, res) {
	req.session.destroy();
	req.logout();
  passportConfig.reset(); //reset auth tokens
  res.redirect(config.uaaURL + '/logout?redirect=' + config.appURL);
});

app.get('/favicon.ico', function (req, res) {
	res.send('favicon.ico');
});

// Sample route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
//currently not being used as we are using passport-oauth2-middleware to check if
//token has expired
/*
function ensureAuthenticated(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}
*/

////// error handlers //////
// catch 404 and forward to error handler
app.use(function(err, req, res, next) {
  console.error(err.stack);
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// development error handler - prints stacktrace
if (node_env === 'development') {
	app.use(function(err, req, res, next) {
		if (!res.headersSent) {
			res.status(err.status || 500);
			res.send({
				message: err.message,
				error: err
			});
		}
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	if (!res.headersSent) {
		res.status(err.status || 500);
		res.send({
			message: err.message,
			error: {}
		});
	}
});

module.exports = app;
